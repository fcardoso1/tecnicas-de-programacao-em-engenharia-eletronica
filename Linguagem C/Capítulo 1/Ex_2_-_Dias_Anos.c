#include <stdio.h>

/* Ex 2 - Transforma dias em anos*/

int main ()
{
    int Dias; /* Declaracao de Variaveis */
    float Anos;
    printf ("Entre com o numero de dias: "); /* Entrada de Dados */
    scanf ("%d",&Dias);
    printf ("\n Endereco da variavel dias %x.\n",&Dias);
    printf ("\n Variavel dias %d.\n",Dias);
    Anos=Dias/365.25; /* Conversao Dias->Anos */
    printf ("\n%d dias equivalem a %f anos.\n",Dias,Anos);
    return(0);
}
